<?php

namespace Drupal\display_selected_and_unselected\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of 'display_selected_and_unselected_keys' formatter.
 *
 * @FieldFormatter(
 *   id = "display_selected_and_unselected_keys",
 *   label = @Translation("Display selected and unselected keys"),
 *   field_types = {
 *     "list_string",
 *     "list_integer",
 *     "list_float"
 *   }
 * )
 */
class DisplaySelectedAndUnselectedKeysFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $allowed_values = $this
      ->fieldDefinition
      ->getItemDefinition()
      ->getSettings()['allowed_values'];

    // Allowed number of values.
    $cardinality = $this
      ->fieldDefinition
      ->getFieldStorageDefinition()
      ->getCardinality();

    // If the value equal 1 then we assume that the type of a widget is 'radio'.
    // Otherwise we assume that the type of a widget is checkbox.
    if ($cardinality == 1) {
      $type = 'radio';
    }
    else {
      $type = 'checkbox';
    }

    $selected_keys = [];

    foreach ($items as $delta => $item) {
      $selected_keys[] = $item->value;
    }

    $field_name = $this->fieldDefinition->getName();

    $elements[] = [
      '#theme' => 'display_selected_and_unselected_keys_' . $type,
      '#allowed_values' => $allowed_values,
      '#selected_keys' => $selected_keys,
      '#field_name' => $field_name,
    ];

    return $elements;
  }

}
