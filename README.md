# Display Selected and Unselected


## Introduction

Provides the field formatters to display all selected and unselected values
of a list field.
The module supports the following field types:
List (text), List (float), List (integer).

If 'Allowed number of values' of a field is equal 1, then the field values
rendered as 'radio' elements.
In other cases field values rendered as 'checkboxes'.

The module provides two formatters:
1. 'Display selected and unselected values'
   (can be used for output of values of a list field).

2. 'Display selected and unselected keys'
   (can be used for output of keys of a list field).

Also the module provides Twig templates which can be used for customisation
of the HTML output.

## Requirements

This module does not requires additional modules.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings.
There is no configuration.


## How to Use

1. Visit 'Manage display' tab of your content type.

2. If you need to display values of a list field,
   then select 'Display selected and unselected values' formatter.
   But if you need to display keys, then select
   'Display selected and unselected keys' formatter.

3. Click on the 'Save' button.

## Maintainers

- Andrey Vitushkin - [wombatbuddy](https://www.drupal.org/u/wombatbuddy)

### Author
Andrey Vitushkin <andrey.vitushkin at gmail.com>